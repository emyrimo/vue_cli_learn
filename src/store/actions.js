const actions = {
    increaseAfterSecond({commit}){
        setTimeout(()=>{
         commit('increase');
        }, 1000);
    },
    decreaseAfterSecond({commit}){
        setTimeout(()=>{
         commit('decrease');
        }, 1000);
    },
};

export default actions;