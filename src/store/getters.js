const getters = {
    counter(state){
      return state.counter;
    },
    doubleCounter(state){
        return  state.counter * 2;
    },
    // mulitCounter:(state)=>(multi)=> {
    //     return  state.counter * multi;
    // }
};

export default getters;