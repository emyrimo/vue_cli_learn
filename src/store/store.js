import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        counter:0,
    },
    getters: getters,
    mutations: mutations,
    actions: actions
})

export default store;