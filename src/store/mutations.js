const mutations = {
    increase(state){
         state.counter++;
    },
    decrease(state){
         state.counter--;
    },
    num_decrease(state, num1){
         state.counter -= num1;
    },
};

export default mutations;