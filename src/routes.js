import HomePage from './components/pages/HomePage';
import AboutPage from './components/pages/AboutPage';
import CoursesPage from './components/pages/CoursesPage';
import CoursePage from './components/pages/CoursePage';
import NotFoundPage from './components/pages/NotFoundPage';

const routes = [
	{path: '/', component: HomePage, name: 'home'},
    {path: '/about', component: AboutPage, name: 'about', beforeEnter: (to, from, next)=>{
       console.log('Before Enter');
       next();
    }},
    {path: '/courses', component: CoursesPage, name: 'courses'},
    {path: '/courses/:id', component: CoursePage, props:true, name: 'view-course'},
    {path: '/404', component: NotFoundPage, name: 'NotFoundPage'},

    // {path:'*', redirect: '/404'},
    {path:'*', component: NotFoundPage}
	];

export default routes;